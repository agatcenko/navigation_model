#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <iostream>
#include <vector>
#include <string>

#include "AUV.h"
#include "beacon.h"
#include "parser.h"
#include "model.h"

using namespace boost::numeric::ublas;

int main(int argc, char* argv[])
{
	std::string filename = "settings.xml";
	std::vector<AUV*> auvs = parse(filename);
	std::vector<Beacon*> beacons;
	beacons.push_back(new Beacon()); // TO DO from config
    beacons.push_back(new Beacon());

	Model m(auvs, beacons, "ALL");
	m.run();

	return 0;	
}