#include <stdlib.h>

#include "kalman_filter.h"

void kalman_extrapolation(matrix<double> &X, matrix<double> &P, matrix<double> &D, matrix<double> &Q)
{
	X += D; // Обновление вектора состояния
  	P += Q; // Обновление ковариационной матрицы для вектора состояния
}

void kalman_correction(matrix<double> &X, matrix<double> &P, matrix<double> &H, matrix<double> &Z, 
	matrix<double> &R, matrix<double> &Zexpected)
{
	matrix<double> HT; // Транспонированная матрица измерений
	matrix<double> Y; // Ошибка (рассогласование)
	matrix<double> S; // Ковариационная матрица для вектора ошибки
	matrix<double> K; // Матрица коэффициентов усиления

	HT = trans(H); // Транспонированная матрица измерений

	S = prod(H, P); // Ковариационная матрица для вектора ошибки
	S = prod(S, HT);
	S += R;
	
	matrix<double> S_copy(S);
	permutation_matrix<std::size_t> pm(S_copy.size1());
	lu_factorize(S_copy, pm);
	S.assign(identity_matrix<double>(S_copy.size1()));
	lu_substitute(S_copy, pm, S); //invert matrix

	K = prod(P, HT); // Матрица коэффициентов усиления (оптимальная по Калману)
	K = prod(K, S);
	
	Y = Z - Zexpected; // Ошибка (рассогласование)
	
	S = K; // Коррекция вектора состояния
	S = prod(S, Y);
	
	X += S;
	K = prod(K, H); // Ковариационная матрица для вектора состояния
	K = prod(K, P);
	P -= K;
}