#include "model.h"

using namespace std;

double fixate_time()
{
    struct timeb timebuf;
    ftime(&timebuf);
    return ((double)timebuf.time + (double)timebuf.millitm / 1000);
}

Model::Model(std::vector<AUV*> a, std::vector<Beacon*> b, std::string f)
{
	m = 2;
	n = 1;

    t = 0;
	dt = 0.1;
	t_gans_pf = 0.0;
    t_gans_ekf = 0.0;
	t_start = 0.0;
  	t_stop = 60.0 * 60.0 * 6.0;
	gans_period = 50.0; // 100
	period = 360.0; // 360, 400, 800

	za_err_av = 0.000;
  	za_err_sd = 0.100;

  	fi_err_av = 0.0;
  	fi_err_sd = 0.5 * M_PI / 180;

  	vx_err_av = 0.0;
  	vx_err_sd = 0.050;

  	vy_err_av = 0.0;
  	vy_err_sd = 0.050;

	H.resize(n, m);
	Z.resize(n, 1);
	R.resize(n, n);
	Zexpected.resize(n, 1);

	p_number = 1000;

	auvs = a;
	beacons = b;
	particles.resize(p_number);
	observation = false;

	srand(std::chrono::system_clock::now().time_since_epoch().count());

	frame_width = 600;
	frame_height = 600;
	draw_radius = 3;
	auv_color = CV_RGB(255, 0, 0);
  	beacon_color = CV_RGB(0, 255, 0);
  	particle_color = cv::Vec3b(255, 0, 0);

  	filter = f;
  	EKF = PF = false;
    pf_time = ekf_time = 0;

  	if (filter == "EKF") {
  		EKF = true;
  		std::cout << "EKF choosen!" << std::endl;
  	}
  	if (filter == "PF") {
  		PF = true;
  		std::cout << "PF choosen!" << std::endl;
  	}
    if (filter == "ALL") {
        EKF = PF = true;
        std::cout << "PF and EKF choosen!" << std::endl;
    }

  	if (!(EKF || PF))
  		std::cout << "Filter not choosen!" << std::endl;	
}

void Model::AUV_motion(AUV* auv) // Может быть дополнительные аргументы
{
	// Модель движения АНПА (закон изменения координат)
	auv->fi_true = 0.0;
	auv->vx_true = 1.0;
    auv->vy_true = 0.0;

    auv->xa_true = auv->vx_true * (t - t_start);
    auv->ya_true = 0.0;
    auv->za_true = 200.0;

    // Получение измерений
    auv->za_meas = auv->za_true + normal_distr(za_err_av, za_err_sd);
    auv->fi_meas = auv->fi_true + normal_distr(fi_err_av, fi_err_sd);
    auv->vx_meas = auv->vx_true + normal_distr(vx_err_av, vx_err_sd);
    auv->vy_meas = auv->vy_true + normal_distr(vy_err_av, vy_err_sd);
}

void Model::beacon_adaptive_motion(Beacon* beacon, AUV* auv, double A, double B, double C, double xa, double ya)
{
    double R = auv->za_meas - beacon->zb_meas; // Желаемая горизонтальная дистанция от АНПА до маяка

    double xb_1, yb_1;
    double xb_2, yb_2;

    double xb_opt, yb_opt, fb_opt;

    double phi;

    double vb_max = 4.0;

    // Оценка будущего положения АНПА через время t
    xa += auv->vx_meas * cos(auv->fi_meas) * gans_period;
    ya += auv->vy_meas * sin(auv->fi_meas) * gans_period;

    // Анализ ошибки определения местоположения АНПА
    if (C == 0 && A == B) {
        // Эллипс ошибки вырожден в круг, можно находиться там, где нам удобней

        // Оптимальный курс
        fb_opt = atan2(ya - beacon->yb_meas, xa - beacon->xb_meas);

        // Ближайшая точка, отвечающая критерию оптимальности
        xb_opt = xa - R * cos(fb_opt);
        yb_opt = ya - R * sin(fb_opt);
    }
    else {
        // Оптимальный азимут на маяк
        phi = atan2(2 * C, A - B) / 2;

        // Выбор оптимальной точки по критерию минимального расстояния
        xb_1 = xa - R * cos(phi);
        yb_1 = ya - R * sin(phi);

        xb_2 = xa + R * cos(phi);
        yb_2 = ya + R * sin(phi);

        if (sqr(xb_1 - beacon->xb_meas) + sqr(yb_1 - beacon->yb_meas) <
            sqr(xb_2 - beacon->xb_meas) + sqr(yb_2 - beacon->yb_meas)) {
            xb_opt = xb_1;
            yb_opt = yb_1;
        }
        else {
            xb_opt = xb_2;
            yb_opt = yb_2;
        }

        // Оптимальный курс движения маяка
        fb_opt = atan2(yb_opt - beacon->yb_meas, xb_opt - beacon->xb_meas);
    }

    beacon->fb_true = fb_opt;

    // Перемещение маяка
    // Если оптимальная точка достижима, то маяк в ней
    if (sqrt(sqr(xb_opt - beacon->xb_meas) + sqr(yb_opt - beacon->yb_meas)) <= vb_max * gans_period) {
        beacon->xb_true = xb_opt;
        beacon->yb_true = yb_opt;
    }
    else {
        // Иначе - приблизились настолько, насколько успели
        beacon->xb_true += vb_max * gans_period * cos(fb_opt);
        beacon->yb_true += vb_max * gans_period * sin(fb_opt);
    }

    beacon->xb_meas = beacon->xb_true + normal_distr(0.0, 0.01);
    beacon->yb_meas = beacon->yb_true + normal_distr(0.0, 0.01);
}

void Model::beacon_motion(Beacon* beacon, AUV* auv)
{
	// Модель движения маяка (закон изменения координат)
    beacon->xb_true = auv->xa_true + 200 * sin(2 * M_PI / period * t); // + normal_distr(0, 3.0);
    beacon->yb_true = auv->ya_true + 200 * cos(2 * M_PI / period * t); // + normal_distr(0, 3.0);
    
  	beacon->zb_true = 1.0;

  	beacon->zb_meas = beacon->zb_true; // + normal_distr(zb_err_av, zb_err_sd);
    beacon->xb_meas = beacon->xb_true;
    beacon->yb_meas = beacon->yb_true;
}

void Model::output_header_pf(std::ofstream &out)
{
	out << "T\tX_true\tY_true\tX_calc\tY_calc\tX_true - X_calc\tY_true - Y_calc\tweighted_d\tis_resampling\t";
	out << "X_best\tY_best\tX_beacon\tY_beacon\tA\tB\tC";
    out << std::endl;
}

void Model::output_header_ekf(std::ofstream &out)
{
	out << "T\tX_true\tY_true\tXb\tYb\tX_dr\tY_dr\tX_Kalman\tY_Kalman\tErrX_dr\tErrY_dr\tErr_dr\tErrX_Kalman\t";
	out << "ErrY_Kalman\tErr_Kalman\tVarX\tVarY\tSKO\tA\tB\tC\n";
}

void Model::output_results_pf(Beacon* beacon, AUV* auv, std::ofstream &out)
{
	out << t << "\t" << auv->xa_true << "\t" << auv->ya_true << "\t" << eval_x << "\t" << eval_y << "\t" <<
	auv->xa_true - eval_x << "\t" << auv->ya_true - eval_y << "\t" << weighted_dist << "\t" << 
    resampling_flag << "\t" << pf_best_x << "\t" << pf_best_y << "\t" << beacon->xb_true << "\t" << beacon->yb_true
    << "\t"<< particle_A << "\t" << particle_B << "\t" << particle_C << std::endl;
}

void Model::output_results_ekf(Beacon* beacon, AUV* auv, std::ofstream &out)
{
	out << t << "\t" << auv->xa_true << "\t" << auv->ya_true << "\t" << beacon->xb_true << "\t" <<
	beacon->yb_true << "\t" << auv->xa_dr << "\t" << auv->ya_dr << "\t" << auv->x << "\t" << auv->y << "\t" <<
	auv->xa_dr - auv->xa_true << "\t" << auv->ya_dr - auv->ya_true << "\t" << 
	sqrt(sqr(auv->xa_dr - auv->xa_true) + sqr(auv->ya_dr - auv->ya_true)) << "\t" <<
	auv->x - auv->xa_true << "\t" << auv->y - auv->ya_true << "\t" <<
	sqrt(sqr(auv->x - auv->xa_true) + sqr(auv->y - auv->ya_true)) << "\t" << auv->sx << "\t" << auv->sy << "\t" <<
	sqrt(auv->sx + auv->sy) << "\t" << auv->P(0, 0) << "\t" << auv->P(1, 1) << "\t" << auv->P(0, 1) <<std::endl;	
}

void Model::draw(Beacon* beacon, AUV* auv, bool current_moment)
{
	if (current_moment)
		frame = cv::Mat::ones(frame_width, frame_height, CV_8UC3);

	for (size_t i = 0; i < particles.size(); ++i) {
		frame.at<cv::Vec3b>(frame_height / 2 + (int)particles[i].y % frame_height, 40 + (int)particles[i].x % frame_width) = particle_color;
	}

	cv::circle(frame, cvPoint(40 + (int)auv->xa_true % frame_width, 
		frame_height / 2 + (int)auv->ya_true % frame_height), draw_radius, auv_color, -1, 8, 0);
	cv::circle(frame,cvPoint(40 + (int)beacon->xb_true % frame_width, 
		frame_height / 2 + (int)beacon->yb_true % frame_height), draw_radius, beacon_color, 1, 8, 0);
	
	cv::imshow("frame", frame);
	cv::waitKey(15);
}

void Model::calculate_position_pf(Beacon* beacon, AUV* auv, std::ofstream &out_pf)
{
    particles_motion(auv, particles);
    
    if (t >= t_gans_pf) {
        observation = true;
        //beacon_motion(beacon, auv);

        r = sqrt(sqr(auv->xa_true - beacon->xb_true) + sqr(auv->ya_true - beacon->yb_true) + sqr(auv->za_true - beacon->zb_true));

        gans_sd = 0.001 * r;

        // Измеренная дистанция до маяка (добавляется белый шум)
        r += normal_distr(0, gans_sd); // !!!!!
        //r += normal_distr(0, 0.1);

        r = sqrt(sqr(r) - sqr(auv->za_meas - beacon->zb_meas)); // Избавляемся от вертикальной составляющей

        while (t >= t_gans_pf)
            t_gans_pf += gans_period;
    }

    auv->dx = (auv->vx_meas * cos(auv->fi_meas) - auv->vy_meas * sin(auv->fi_meas)) * dt;
    auv->dy = (auv->vx_meas * sin(auv->fi_meas) + auv->vy_meas * cos(auv->fi_meas)) * dt;

    // Случайный шум, обеспечивающий дрейф ССП
    auv->dx += normal_distr(0.002, 0.002) * dt; // !!!!!
    auv->dy += normal_distr(0.002, 0.002) * dt;

    // Координаты только по ССП
    auv->xa_dr += auv->dx;
    auv->ya_dr += auv->dy;

    if (observation) {
        calculate_probability(beacon, particles, r);
    
        double all_sqr_w = 0.0;
        for (size_t i = 0; i < particles.size(); ++i) {
            all_sqr_w += sqr(particles[i].w);
        }

        if (1.0 / all_sqr_w < 650) {
            resampling(particles);
            resampling_flag = true;
        }
    }
    //draw(beacon, auv, true);

    if (observation) {
        int max_weighted = 0;
        calculate_error(auv, particles, weighted_dist, eval_x, eval_y, max_weighted);
    
        pf_best_x = particles[max_weighted].x;
        pf_best_y = particles[max_weighted].y;
        output_results_pf(beacon, auv, out_pf);
        double A, B, C;
        calculate_cov_matrix(particles, A, B, C);
        particle_A = A;
        particle_B = B;
        particle_C = C;
        beacon_adaptive_motion(beacon, auv, A, B, C, eval_x, eval_y);
        //cout << "PARTICLE FILTER! A = " << A << " B = " << B << " C = " << C << endl;
        observation = false;
        resampling_flag = false; 
    }
}

void Model::calculate_position_ekf(Beacon* beacon, AUV* auv, std::ofstream &out_ekf)
{
    if (t >= t_gans_ekf) {
        //beacon_motion(beacon, auv);

        r = sqrt(sqr(auv->xa_true - beacon->xb_true) + sqr(auv->ya_true - beacon->yb_true) + sqr(auv->za_true - beacon->zb_true));
        
        gans_sd = 0.001 * r;

        // Измеренная дистанция до маяка (добавляется белый шум)
        r += normal_distr(0, gans_sd); // !!!!!
        //r += normal_distr(0, 0.1);

        // Полученный вектор измерений
        Z(0, 0) = r;

        gans_sd = 0.001 * r; // Это нужно делать 2-й раз ???

        // Ковариационная матрица шума измерений дальностей
        R(0, 0) = sqr(gans_sd);
        //R(0, 0) = sqr(1.0);

        // Предполагаемые координаты
        auv->x = auv->X(0, 0);
        auv->y = auv->X(1, 0);

        // Ожидаемая дистанция до маяка (вычисляется на основе предполагаемых координат)
        r = sqrt(sqr(auv->x - beacon->xb_true) + sqr(auv->y - beacon->yb_true) + sqr(auv->za_meas - beacon->zb_true));

        // Ожидаемый вектор измерений
        Zexpected(0, 0) = r;

        // Матрица измерений в предполагаемой точке (расчитывается на основе предполагаемых (счисленных) координат)
        H(0, 0) = (auv->x - beacon->xb_true) / r;
        H(0, 1) = (auv->y - beacon->yb_true) / r;

        // Этап коррекции
        kalman_correction(auv->X, auv->P, H, Z, R, Zexpected);

        auv->x = auv->X(0, 0);
        auv->y = auv->X(1, 0);

        auv->sx = auv->P(0, 0);
        auv->sy = auv->P(1, 1);

        output_results_ekf(beacon, auv, out_ekf);

        // Доверительный интервал (плюс-минус три сигма)
        auv->sx = 3 * sqrt(auv->sx);
        auv->sy = 3 * sqrt(auv->sy);

        if ((auv->xa_true < auv->x - auv->sx) || (auv->xa_true > auv->x + auv->sx) || 
            (auv->ya_true < auv->y - auv->sy) || (auv->ya_true > auv->y + auv->sy))
                cout << "Real position outside of the 3-sigma region!!!" << endl;

        // Определение момента получения очередного измерения дистанции до маяка
        while (t >= t_gans_ekf)
            t_gans_ekf += gans_period;
        
        beacon_adaptive_motion(beacon, auv, auv->P(0, 0), auv->P(1, 1), auv->P(0, 1), auv->x, auv->y);

        //cout << "KALMAN FILTER! A = " << auv->P(0, 0) << " B = " << auv->P(1, 1) << " C = " << auv->P(0, 1) << endl;
    }

    // Счисление координат, которые "должны быть" на следующем такте работы ССП

    auv->dx = (auv->vx_meas * cos(auv->fi_meas) - auv->vy_meas * sin(auv->fi_meas)) * dt;
    auv->dy = (auv->vx_meas * sin(auv->fi_meas) + auv->vy_meas * cos(auv->fi_meas)) * dt;
    
    // Случайный шум, обеспечивающий дрейф ССП
    auv->dx += normal_distr(0.002, 0.002) * dt; // !!!!!
    auv->dy += normal_distr(0.002, 0.002) * dt;

    // Координаты только по ССП
    auv->xa_dr += auv->dx;
    auv->ya_dr += auv->dy;
    
    // Вектор перемещения (зашумленный)
    auv->D(0, 0) = auv->dx;
    auv->D(1, 0) = auv->dy;

    // Ковариационная матрица для вектора перемещения
    auv->Q(0, 0) = sqr(0.050 * dt);
    auv->Q(0, 1) = 0.000;
    auv->Q(1, 0) = 0.000;
    auv->Q(1, 1) = sqr(0.050 * dt);

    // Этап экстраполяции
    kalman_extrapolation(auv->X, auv->P, auv->D, auv->Q);
}

void Model::run()
{
	std::ofstream out_ekf("results_ekf.txt");
    std::ofstream out_pf("results_pf.txt");
	if (EKF)
		output_header_ekf(out_ekf);
	if (PF)
		output_header_pf(out_pf);

	frame = cv::Mat::ones(frame_width, frame_height, CV_8UC3);
    if (PF) {
        beacon_motion(beacons[0], auvs[0]);
        //beacon_adaptive_motion(beacons[0], auvs[0],auvs[0]->P(0, 0), auvs[0]->P(1, 1), 
        //                    auvs[0]->P(0, 1), auvs[0]->xa_dr, auvs[0]->ya_dr);
    }
    if(EKF) {
        beacon_motion(beacons[1], auvs[1]);
        //beacon_adaptive_motion(beacons[1], auvs[1], auvs[1]->P(0, 0), auvs[1]->P(1, 1), 
        //                    auvs[1]->P(0, 1), auvs[1]->xa_dr, auvs[1]->ya_dr);
    } 
        
	init_particles(beacons[0], auvs[0], particles); // Создание частиц

	// Сделать проверку на наличие маяков и аппаратов ???
	for (t = t_start; t <= t_stop; t += dt) {
        for (size_t i = 0; i < auvs.size(); ++i)
            AUV_motion(auvs[i]);

        if (PF)
            calculate_position_pf(beacons[0], auvs[0], out_pf);
        if (EKF)
            calculate_position_ekf(beacons[1], auvs[1], out_ekf);
	}
}
