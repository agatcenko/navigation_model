#ifndef BEACON_H
#define BEACON_H

class Beacon {
public:
	double xb_true; // Истинные координаты маяка
  	double yb_true;
  	double zb_true;
    double fb_true; // курс

  	double xb_meas; // Измеренные координаты маяка
  	double yb_meas;
  	double zb_meas;

  	Beacon() : zb_true(0.0) {};
};

#endif // BEACON_H