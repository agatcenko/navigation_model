#ifndef DISTRIBUTION_H
#define DISTRIBUTION_H

#include <random>
#include <chrono>

double sqr(double a);

double random01();

double random_interval(double value);

double randG(double mean, double std_dev);

double normal_distr(double mean, double std_dev);

#endif // DISTRIBUTION