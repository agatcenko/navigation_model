#ifndef AUV_H
#define AUV_H

#include <boost/numeric/ublas/matrix.hpp>

using namespace boost::numeric::ublas;

class AUV {
	
public:
	double xa_true; // Истинные координаты АНПА, м
	double ya_true;
	double za_true;

	double xa_true_start; // Начальные координаты АНПА, м
	double ya_true_start;
	double za_true_start;

	double xa_dr_start; // Предполагаемые начальные координаты АНПА, м
	double ya_dr_start;

	double xa_dr; // Предполагаемые (счисленные) координаты АНПА, м
	double ya_dr;

	double dx; // Перемещение АНПА
	double dy;

	double x; // Координаты АНПА, полученные с помощью фильтра Калмана
	double y;

	double fi_true; // Истинный курс АНПА, градусы
	double vx_true; // Истинное значение продольной скорости АНПА, м/с
	double vy_true; // Истинное значение поперечной скорости АНПА, м/с

	double za_meas; // Измеренное значение глубины АНП, м
	double fi_meas; // Измеренное значение курса АНПА, градусы
	double vx_meas; // Измеренное значение продольной скорости АНПА, м/с
	double vy_meas; // Измеренное значение поперечной скорости АНПА, м/с

	double sx; // Среднеквадратические отклонения (из ковариационной матрицы)
  	double sy;

  	unsigned int m; // Размерность вектора состояния

  	matrix<double> X; // Вектор состояния
  	matrix<double> P; // Ковариационная матрица для вектора состояния
	matrix<double> D; // Вектор перемещения
  	matrix<double> Q; // Ковариационная матрица для вектора перемещения

	AUV(double x = 0.0, double y = 0.0, double z = 0.0);
};

#endif // AUV_H