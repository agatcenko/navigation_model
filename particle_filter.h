#ifndef PARTICLE_FILTER_H
#define PARTICLE_FILTER_H

#include <vector>

#include "AUV.h"
#include "beacon.h"
#include "distribution.h"

struct Particle {
    double x;
    double y;
    double w; // weight
    Particle() : x(0.0), y(0.0), w(0.0) {}
    Particle(double _x, double _y, double _w) : x(_x), y(_y), w(_w) {}
};

void init_particles(Beacon* beacon, AUV* auv, std::vector<Particle> &particles);

void particles_motion(AUV* auv, std::vector<Particle> &particles);

void calculate_error(AUV* auv, std::vector<Particle> &particles, double &weighted_dist, double &eval_x, double &eval_y, int &max_weigted);

void calculate_probability(Beacon* beacon, std::vector<Particle> &particles, double measurement);

double gaussian(double mean, double sigma, double x);

void normalize_weights(std::vector<Particle> &particles);

void resampling(std::vector<Particle> &particles);

void calculate_cov_matrix(std::vector<Particle> &particles, double &A, double &B, double &C);

#endif // PARTICLE_FILTER