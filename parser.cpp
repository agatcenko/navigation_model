#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>

#include "parser.h"

std::vector<AUV*> parse(std::string &s)
{
	using boost::property_tree::ptree;
	ptree pt;
	read_xml(s, pt);

	std::vector<AUV*> auvs;
	BOOST_FOREACH(ptree::value_type const& v, pt.get_child("settings")) {
		if (v.first == "auv") {
			double x = v.second.get<double>("x_start");
			double y = v.second.get<double>("y_start");
			double z = v.second.get<double>("z_start");
			auvs.push_back(new AUV(x, y, z));
		}
	}

	return auvs;
}