#include "math.h"

#include "particle_filter.h"

#include <map>

void init_particles(Beacon* beacon, AUV* auv, std::vector<Particle> &particles)
{
	int init_time = 60 * 10;
	double r_sum = 0.0, r_min, r_max;
	for (int i = 0; i < init_time; ++i) {
		double r = sqrt(sqr(auv->xa_true - beacon->xb_true) + sqr(auv->ya_true - beacon->yb_true));
		double gans_sd = 0.001 * r;
	    r += normal_distr(0, gans_sd);
	    
	    if (i == 0) {
	    	r_min = r_max = r;
	    }
	    else {
	    	if (r < r_min)
	    		r_min = r;
	    	if (r > r_max)
	    		r_max = r;
	    }

	    r_sum += r;
	}
	
	double r_init = r_sum / init_time;
	for (size_t i = 0; i < particles.size(); ++i) {
		Particle p;
		double fi = random_interval(M_PI);
		p.x = beacon->xb_true + (r_init + random_interval(r_max - r_min)) * cos(fi);
		p.y = beacon->yb_true + (r_init + random_interval(r_max - r_min)) * sin(fi);
		p.w = 1.0 / particles.size();
		particles[i] = p;
	}
}

void particles_motion(AUV* auv, std::vector<Particle> &particles)
{
	auv->fi_true *= M_PI / 180.0;
	for (size_t i = 0; i < particles.size(); ++i) {
		//particles[i].x += (auv->vx_true * cos(auv->fi_true) - auv->vy_true * sin(auv->fi_true)) * 0.1;
		//particles[i].y += (auv->vx_true * sin(auv->fi_true) + auv->vy_true * cos(auv->fi_true)) * 0.1;
    	particles[i].x += auv->dx + normal_distr(0.0, 0.002); // !!!!!
		particles[i].y += auv->dy + normal_distr(0.0, 0.002);
	}
}

void calculate_error(AUV* auv, std::vector<Particle> &particles, double &weighted_dist, double &eval_x, double &eval_y, int &max_weigted)
{
	weighted_dist = 0.0; // дистанция с учетом весов

	eval_x = 0.0, eval_y = 0.0;
	double max_w = 0.0;
	for (size_t i = 0; i < particles.size(); ++i) {
		double dx = particles[i].x - auv->xa_true;
		double dy = particles[i].y - auv->ya_true;
		double d = sqrt(sqr(dx) + sqr(dy));
		
		weighted_dist += d * particles[i].w;
		eval_x += particles[i].x * particles[i].w;
		eval_y += particles[i].y * particles[i].w;

		if (particles[i].w > max_w) {
			max_w = particles[i].w;
			max_weigted = i;
		}
	}
}

double gaussian(double mean, double sigma, double x)
{
	return exp(-sqr(mean - x) / sqr(sigma) / 2.0) / sqrt(2.0 * M_PI * sqr(sigma));
}

void calculate_probability(Beacon* beacon, std::vector<Particle> &particles, double measurement)
{
	for (size_t i = 0; i < particles.size(); ++i) {
		double dist = sqrt(sqr(particles[i].x - beacon->xb_true) + sqr(particles[i].y - beacon->yb_true));
		particles[i].w *= gaussian(dist, 0.2, measurement); // 1.0 - может и нет)
	}
 
	normalize_weights(particles);
}

void normalize_weights(std::vector<Particle> &particles)
{
	double sum_w = 0.0;
	for (size_t i = 0; i < particles.size(); ++i)
		sum_w += particles[i].w;
	
	for (size_t i = 0; i < particles.size(); ++i) // normalize
		particles[i].w /= sum_w;
}

void resampling(std::vector<Particle> &particles)
{
	double beta = 0.0;
	double max_w = 0.0, sum_w = 0.0;
	std::vector<double> distribution;

	// try multinominal resampling
	
	for (size_t i = 0; i < particles.size(); ++i) {
		sum_w += particles[i].w;
		distribution.push_back(sum_w); // generate stripe with sections, which lengths are proportionsal weights
	}
	std::vector<Particle> new_particles;

	for (size_t i = 0; i < particles.size(); ++i) {
		double p = (double)rand() / RAND_MAX; // [0-1]
		int index = 0;
		for (size_t j = 0; j < distribution.size() - 1; ++j) {
			if (p < distribution[0]) {
				index = 0;
				break;
			}
			if (p > distribution[j] && p <= distribution[j + 1]) {
				index = j + 1;
				break;
			}
		}
		new_particles.push_back(Particle(particles[index].x + random_interval(1.0), 
								particles[index].y + random_interval(1.0), 
								particles[index].w));
		//new_particles.push_back(particles[index]);
	}
	
	particles = new_particles;

	normalize_weights(particles);
}

void calculate_cov_matrix(std::vector<Particle> &particles, double &A, double &B, double &C)
{
	double mx = 0, my = 0; // матожидания
	A = B = C = 0;
	for (unsigned int i = 0; i < particles.size(); ++i) {
		double w_x = particles[i].w * particles[i].x;
		double w_y = particles[i].w * particles[i].y;

		mx += w_x;
		my += w_y;

		A += w_x * particles[i].x;
		B += w_y * particles[i].y;
		C += w_x * particles[i].y; 
	}

	A -= mx * mx;
	B -= my * my;
	C -= mx * my;
}
