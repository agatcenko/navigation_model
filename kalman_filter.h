#ifndef KALMAN_FILTER_H
#define KALMAN_FILTER_H

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp> // for invert matrix

using namespace boost::numeric::ublas;

void kalman_extrapolation(matrix<double> &X, matrix<double> &P, matrix<double> &D, matrix<double> &Q);

void kalman_correction(matrix<double> &X, matrix<double> &P, matrix<double> &H, matrix<double> &Z, 
	matrix<double> &R, matrix<double> &Zexpected);

#endif // KALMAN_FILTER