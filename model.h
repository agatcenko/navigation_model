#ifndef MODEL_H
#define MODEL_H

#include <boost/numeric/ublas/matrix.hpp>
#include <opencv2/opencv.hpp>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <time.h>
#include <sys/timeb.h>

#include "AUV.h"
#include "beacon.h"
#include "distribution.h"
#include "kalman_filter.h"
#include "particle_filter.h"

using namespace boost::numeric::ublas;

class Model { 

  	double t_gans_pf;
    double t_gans_ekf;
  	double gans_period; // Период работы ГАНС
  	double period; // Период обращения маяка вокруг АНПА
  	double r; // Дистанция до маяка
  	double w; // Угловая скорость АНПА по курсу
  	double gans_sd; // Среднеквадратическое отклонение ГАНС

  	unsigned int m; // Размерность вектора состояния
  	unsigned int n; // Размерность вектора измерений

	// Задание параметров шумов (нормальное распределение): среднее значение, СКО
	double za_err_av; // Параметры шума измерения глубины АНПА, м
	double za_err_sd;

	double fi_err_av; // Параметры шума измерения курса АНПА, градусы
	double fi_err_sd;

	double vx_err_av; // Параметры шума измерения продольной скорости АНПА, м/с
	double vx_err_sd;

	double vy_err_av; // Параметры шума измерения поперечной скорости АНПА, м/с
	double vy_err_sd;

	// Задание временных параметров моделирования
	double t; // Время, с
	double t_start; // Момент начала моделирования, с
	double t_stop; // Момент окончания моделирования, с
	double dt; // Период работы системы счисления пути (ССП) АНПА, с

	double za_err; // Ошибка измерения глубины АНПА, м
	double fi_err; // Ошибка измерения курса АНПА, градусы
	double vx_err; // Ошибка измерения продольной скорости АНПА, м/с
	double vy_err; // Ошибка измерения поперечной скорости АНПА, м/с

	double eval_x;
	double eval_y; // Оцененные координаты аппарата

    double pf_best_x;
    double pf_best_y; // Точка с максимальным весом

	double average_dist; // без учета веса, средняя дистанция 
	double weighted_dist; // дистанция с учетом весов

	double robust_mean_x;
	double robust_mean_y; // оценка координат, с исп-ем точек, вблизи точек с наибольшим весом
	
	matrix<double> H; // Матрица измерений
	matrix<double> Z; // Полученный вектор измерений
	matrix<double> R; // Ковариационная матрица шума измерений
	matrix<double> Zexpected; // Ожидаемый вектор измерений

	int p_number; // num of particles
	bool observation;

	std::vector<AUV*> auvs;
	std::vector<Beacon*> beacons;
	std::vector<Particle> particles;

	cv::Mat frame;
	int frame_width;
	int frame_height;
	int draw_radius;
	cv::Scalar auv_color;
	cv::Scalar beacon_color;
	cv::Vec3b particle_color;

    std::string filter; // Используемый фильтр
    bool EKF; // expand kalman filter
    bool PF; // particle filter

    double pf_time;
    double ekf_time;
    bool resampling_flag;

    double particle_A;
    double particle_B;
    double particle_C;

	void AUV_motion(AUV* auv);
	void beacon_motion(Beacon* beacon, AUV* auv);
    void beacon_adaptive_motion(Beacon* beacon, AUV* auv, double A, double B, double C, double xa, double ya);
	void calculate_position_pf(Beacon* beacon, AUV* auv, std::ofstream &out_pf);
    void calculate_position_ekf(Beacon* beacon, AUV* auv, std::ofstream &out_ekf);
	void draw(Beacon* beacon, AUV* auv, bool current_moment = true);
	
	void output_header_pf(std::ofstream &out);
    void output_header_ekf(std::ofstream &out);
    void output_results_pf(Beacon* beacon, AUV* auv, std::ofstream &out);
	void output_results_ekf(Beacon* beacon, AUV* auv, std::ofstream &out);
public:
	Model(std::vector<AUV*> a, std::vector<Beacon*> b, std::string f);
	void run();
};

double fixate_time();

#endif // MODEL_H