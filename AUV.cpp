#include "AUV.h"

AUV::AUV(double x, double y, double z)
{
    xa_true_start = x;
    ya_true_start = y;
    za_true_start = z;

    xa_true = xa_true_start;
    ya_true = ya_true_start;
    za_true = za_true_start;

    xa_dr_start = xa_true_start;
    ya_dr_start = ya_true_start;

    xa_dr = xa_true_start;
	ya_dr = ya_true_start;

    fi_true = 0.0;
    vx_true = 0.0;
    vy_true = 0.0;

    za_meas = za_true_start;
    fi_meas = 0.0;
    vx_meas = 0.0;
    vy_meas = 0.0;

	m = 2;
	X.resize(m, 1);
	P.resize(m, m);
	D.resize(m, 1);
	Q.resize(m, m);

	X(0, 0) = xa_dr;
  	X(1, 0) = ya_dr;

   	P(0, 0) = 100.0;
   	P(0, 1) = 0.00;
   	P(1, 0) = 0.00;
   	P(1, 1) = 100.0; 
}