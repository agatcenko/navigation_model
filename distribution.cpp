#include <stdlib.h>
#include <math.h>

#include "distribution.h"

double sqr(double a)
{
    return (a) * (a);
}

double random01()
{
    return (2 * (((double)rand()) / RAND_MAX) - 1);
}

double random_interval(double value) // return random from [-value, value]
{
    double f = (double)rand() / RAND_MAX;
    return f * 2 * value - value;   
}

double randG(double mean, double std_dev)
{
    double X, S;

    // Marsaglia-Bray algorithm
    do
    {
        X = random01();
        S = sqr(X) + sqr(random01());
    }
    while (S >= 1);

    return (mean + std_dev * sqrt(-2 * log (S) / S) * X);
}

double normal_distr(double mean, double std_dev)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::normal_distribution<double> distribution(mean, std_dev);
    return distribution(generator);
}